program main
	use util_module
	use main_module
	implicit none
! 	variable of no use
	integer ( kind = 4 ) :: B_ss_no_use, M_ss_no_use
	real ( kind = 8 ) :: D_ss_no_use
! 	variable
	integer ( kind = 4 ) :: D_bar, MB, MM, MZS, NB, NM, W, gamma, &
		star, i, k, l, maxit, &
		index, it, count, max_row_guo(1), m_conflict, max_row, info_guo
	integer ( kind = 4 ), allocatable :: ipiv_guo(:)
	real (kind = 8 ) :: P, Y, b_ss, bigB, bigb_conflict, bigM, cf, & 
		eps, eta, f, l_ss, p_ss, pi_ss, r_bar, smallB, small_b, y_ss, smallM, &
		zsnode, maxv, diff
	real ( kind = 8 ), allocatable :: chzeroB(:, :)
	real ( kind = 8 ), allocatable :: chzeroM(:, :)
	real ( kind = 8 ), allocatable :: chzerozs(:, :)
	real ( kind = 8 ), allocatable :: bendzs(:, :), bendB(:, :), &
		bendM(:, :),  B(:, :), M(:, :), &
		XBB(:, :), XBM(:, :), XBZS(:, :), &
		XB_tmp_gqd(:, :), XB(:, :), &
		B_space(:), M_space(:), b_space_conflict(:), &
		B_nodes(:, :), M_nodes(:, :), b_nodes_conflict(:), &
		value_fctd(:, :), policy_vd(:, :), &
		qbyB(:), q(:), &
		i_B(:), i_b_conflict(:), cost(:), y_conflict(:), l_conflict(:), p_conflict(:), &
		iB(:), D(:), temp_valn1(:), &
		policy_fctn(:), policy_vn(:, :), value_fctn(:), value_fct(:), policy_vv(:, :), temp_valn(:), &
		XB_transpose_guo(:, :), XB_product_guo(:, :), XB_inv_guo(:, :), &
		temp_valn2(:), temp_qbyB1(:)

	integer ( kind = 4 ), parameter :: fminsearchbnd_variable_num = 3
  	integer ( kind = 4 ) :: icount, ifault, kcount, konvge, numres
	real ( kind = 8 ) :: reqmin, start(fminsearchbnd_variable_num), LB(fminsearchbnd_variable_num), &
			UB(fminsearchbnd_variable_num), step(fminsearchbnd_variable_num), &
			xmin(fminsearchbnd_variable_num), ynewlo
! 	variable define by guo
! 	integer ( kind = 4 ) :: i
! 	call test_kron()
! 	call test_gaucheby(4)
! 	call test_chebeval()
! 	call test_gauher()
! 	call test_norm()
! 	call test_solve_mbBpe_sim3()
! 	call test_fminsearchbnd()
! 	write(*, *) 'test complete'
! 	read(*, *) k
	call timestamp()
	write(*, *) 'Cash, Long-term Debt, Short-term Debt, Productivity Shocks'
! %------------------ introduce fixed operation costs -----------------------
! %------------------ choose short term debt b ------------------------------
! %----------- choose on grid points when simulate the economy --------------
! %----------- impose collateral constraint on short term debt --------------
! %----------------------- correct goods pricing rule -----------------------
! %----------------------- correct bond pricing rule ------------------------
! %--------------- change 'Nelder-Mead simplex direct search' ---------------
! %--------------- remove interest rate shock -------------------------------
! %--------------- solve at annual frequency --------------------------------
! %--------------- add fixed long-term debt issuance costs ------------------

! Model Parameter Values
gamma = 4         ! the elasticity of substitution between goods
rho_z = 0.5 !0.89;      % persistence of tech shock 
sigma_z = 0.25    ! std dev of tech shock innovation
!rho_r = 0; !0.89;      % persistence of monetary shock 
!sigma_r = 0.00;    % std dev of monetary shock innovation
!rho_zr =0.00;      % correlation between productivity and monetary shocks
r_bar = 0.04      ! average risk-free rate
D_bar = 0         ! min of dividend payment
eta = 0.005           ! exogenous exit rate
cf = 0.000          ! fixed operation costs
eps = 0.355          ! enforcement parameter
beta = (1-eta)*1/(1+r_bar)   ! discount factor
f=0.00501               ! fixed long-term debt issuance costs

! Given aggregate wage. price and output
W = 1                           ! wage, exogenous
P = real(gamma, kind = 8)/(gamma-1)*(1+r_bar)*W ! aggregate price in steady-state
Y = P**(-gamma)                  ! aggregate output in steady-state


! Derive steady state
! B_ss_no_use = 0                               ! long-term debt issuance
! M_ss_no_use = 0                               ! cash holdings
p_ss = real(gamma, kind = 8)/(gamma-1)*(1+r_bar)*W     ! good price
l_ss = (p_ss/P)**(-gamma)*Y             ! labor demand
b_ss = W*l_ss+cf                          ! short-term debt issuance
y_ss = l_ss                            ! output
pi_ss = p_ss*y_ss-(1+r_bar)*(W*l_ss+cf)     ! profit
! D_ss_no_use = pi_ss                           ! dividend 

! PEA Parameter Values
MB = 60 ! M-point Gauss-Chebychev quadrature for each 'outside' integral (M>=N)
NB = 60 ! Nth root of 'size' of our approx'n, i.e. highest order term will be N-1
MM = 40 
NM = 40 
MZS = 5
NZS = 5 
!MZR = 3;
!NZR = 2; 
H = 5   ! H-point Gauss-Hermite quadrature for 'inside' integral

! Set bounds for the state space: B, M, and shocks
! productivity shock
bigzs = 2*sigma_z/sqrt(1-rho_z**2)
smallzs = -bigzs

! % monetary shock
! %mu_r = log(r_bar);
! %bigzr = 3*sigma_r/sqrt(1-rho_r^2); 
! %smallzr = -bigzr;
! %bigzr = bigzr + mu_r;
! %smallzr = smallzr + mu_r;

! % long term bond B
bigB = 4*b_ss
smallB = 0

! % corporate cash M
bigM = 4*b_ss
smallM = 0

! % short term bond b
bigb_conflict = 2*b_ss
smallb = 0

! % zeros for the M-th order Chebychev polynomial
call gaucheby(chzerozs, MZS)
! %chzerozr = gaucheby(MZR)
call gaucheby(chzeroB, MB)
call gaucheby(chzeroM, MM)

allocate(bendzs(12000, 1), bendB(12000, 1), bendM(12000, 1))
allocate(zs(12000, 1), B(12000, 1), M(12000, 1))
! % orders: zs, zr, B, M
star = 0
do i=1, MZS
	do k=1, MB
		do l=1, MM
			star = star + 1
			bendzs(star, 1) = chzerozs(i, 1)
			bendB(star, 1) = chzeroB(k, 1)
			bendM(star, 1) = chzeroM(l, 1)
		end do
	end do
end do

zs = ( bendzs + 1 ) * ( bigzs - smallzs ) / 2 + smallzs
zs = exp( zs )
B = ( bendB + 1 ) * ( bigB - smallB ) / 2 + smallB
M = ( bendM + 1 ) * ( bigM - smallM ) / 2 + smallM

! % Matrix of Chebychev (tensor product) basis fuc'ns evaluated at the zeros, X (M^5xN^5).
call chebeval(XBB, NB, chzeroB)
call chebeval(XBM, NM, chzeroM)
call chebeval(XBZS, NZS, chzerozs)
! call timestamp()
call kron(XB_tmp_gqd, XBZS, XBB)
call kron(XB, XB_tmp_gqd, XBM)
! call timestamp()

! % weights and abscissas for conditional expectations (two shocks, prod. and
! % interest rate)
call gauher(x, w_conflict, H)
x = x(H:1:-1)
w_conflict = w_conflict(H:1:-1)

maxit = 900

! %initial guess at v-fuc'n parameters
! %anew = ones(NB*NM*NZS,1);
! %load model_elas_r2_shock
! %load model_elas_gamma5
! load mbB_benchmark_sim2_60405_eps34
call read_file(12000, anew)
! %aold=anew;
! %anew = zeros(NB*NM*NZS,1);
! %anew(1:2000,1)=aold(1:2000,1);
! %load BMb_pe_ss
! %load mbB_pe_bigshock_ss
! %load mbB_pe_ss
! %load mbB_pe_iidzs

! % Create the control space 
! % long-term Bond
B_size = 20
allocate( B_space(B_size) )
B_space = bigB
do i=1, B_size-1
	B_space(B_size-i) = smallB+(bigB-smallB)*(B_size-i-1)/(B_size-1)
end do

! % cash
M_size = 20
allocate( M_space(M_size) )
M_space = bigM
do i=1, M_size-1
	M_space(B_size-i) = bigM*(M_size-i-1)/(M_size-1)
end do

! % short-term debt  
b_size_conflict = 10
allocate( b_space_conflict(b_size_conflict) )
b_space_conflict = bigb_conflict
do i=1, b_size_conflict-1
    b_space_conflict(b_size_conflict-i) = bigb_conflict*(b_size_conflict-i-1)/(b_size_conflict-1)
end do

! %%%% compute abscissas and Chebyshev value for control space: B_space and M_space:
allocate( B_nodes(B_size, 1), M_nodes(M_size, 1), b_nodes_conflict(b_size_conflict) )
B_nodes(:, 1) = 2*(B_space-B_space(1))/(B_space(size(B_space))-B_space(1))-1
M_nodes(:, 1) = 2*(M_space-M_space(1))/(M_space(size(M_space))-M_space(1))-1
b_nodes_conflict = 2*(b_space_conflict-b_space_conflict(1))/(b_space_conflict(size(b_space_conflict))-b_space_conflict(1))-1

allocate( control_space(B_size*M_size*b_size_conflict, 3) )
control_space = .0

do i=1, B_size
	do l=1, M_size
		do k=1, b_size_conflict
			index = (i-1)*M_size*b_size_conflict+(l-1)*b_size_conflict+k
			control_space(index, :) = &
				(/B_space(i), M_space(l), b_space_conflict(k)/)
		end do
	end do
end do

call chebeval(B_control, NB, B_nodes)
call chebeval(M_control, NM, M_nodes)
allocate( b_control_conflict(size(b_nodes_conflict), 1) )
b_control_conflict = 1.0

! allocate( aold(12000) )
allocate( value_fctd(MB*MM*MZS, 1) )
allocate( policy_vd(MB*MM*MZS, 7) )
allocate( temp_qbyB1(B_size*M_size*b_size_conflict), qbyB(B_size*M_size*b_size_conflict) )
allocate( q(B_size*M_size*b_size_conflict) )
allocate( i_B(4000), i_b_conflict(4000), cost(4000) ) ! should be type of integer
allocate( p_conflict(4000), y_conflict(4000), l_conflict(4000) )
allocate( iB(4000), D(4000), temp_valn1(4000), temp_valn2(4000) )
allocate( policy_fctn(12000), policy_vn(12000, 7), value_fctn(12000) )
allocate( value_fct(12000), policy_vv(12000, 7), temp_valn(4000) )
allocate( XB_transpose_guo(12000, 12000), XB_product_guo(12000, 12000), ipiv_guo(12000) )
! do it=1, 1
! 	count = it
! 	aold(:) = anew(:)

! 	write(*, *) 'totally ', MB*MM*MZS, 'iterations.'

! !$OMP PARALLEL PRIVATE(i, qbyB, i_b_conflict, i_B, cost, q, temp_qbyB1, temp_valn2) &
! !$OMP 			PRIVATE(p_conflict, y_conflict, l_conflict, iB, D) &
! !$OMP 			PRIVATE(temp_valn1, temp_valn, maxv, max_row_guo, max_row)
! !$OMP DO
! 	do i=1, MB*MM*MZS
! 		value_fctd(i, 1) = 0
! 		policy_vd(i, :) = 0
! 		temp_qbyB1 = 0
! 		temp_valn2 = 0


! 		call guo_223_314(i, temp_qbyB1, temp_valn2)		
! ! 			!$OMP SECTIONS
! ! 				!$OMP SECTION
! ! 				call guo_223(i, temp_qbyB1)
! ! 				!$OMP SECTION
! ! 				call guo_314(i, temp_valn2)
! ! 			!$OMP END SECTIONS

! 		temp_qbyB1 = temp_qbyB1 / sqrt( pi )
! 		qbyB = temp_qbyB1 * beta


! 		where ( control_space(:,1)==0 ) qbyB(:) = 0
! 		q = qbyB / control_space(:, 1)
! 		where ( control_space(:,1)==0 ) q(:) = 1d-3
! 		where ( q(:)==0 ) q(:) = 1d-3

! 		i_b_conflict = 0
! 		i_B = 0
! 		where ( control_space(:, 3) > 0 ) i_b_conflict = 1
! 		where ( control_space(:, 1) > 0 ) i_B = 1
! 		cost = r_bar*i_b_conflict*(1-i_B)+(1.0/q-1)*i_B
! 		p_conflict = gamma*1.0/(gamma-1.0)*(1+cost)*W/zs(i, 1)
! 		y_conflict = (P**gamma)*Y*(p_conflict**(-gamma))
! 		l_conflict = y_conflict / zs(i, 1)

! 		iB = 0
! 		where ( qbyB > 0 ) iB = 1
! 		D = p_conflict*y_conflict+(M(i, 1)+qbyB+control_space(:,3)- &
! 			W*l_conflict-cf-f*iB)-(1+r_bar)*control_space(:,3) - &
!             B(i, 1)-control_space(:,2)

!         temp_valn1 = D

! 		temp_valn2 = temp_valn2 / sqrt ( pi )
! 		temp_valn = temp_valn1 + temp_valn2*beta

! 		where ( D < D_bar ) temp_valn = -huge(0)
! 		where ( ( qbyB+M(i, 1)+control_space(:, 3) &
! 			-W*l_conflict-cf-f*iB ) < 0 ) &
! 			temp_valn = -huge(0)
! 		where ( control_space(:, 3)>eps ) &
! 			temp_valn = -huge(0)

! 		maxv = maxval( temp_valn )
! 		max_row_guo = maxloc( temp_valn )
! 		max_row = max_row_guo(1)
! 		policy_fctn(i) = max_row

! 		policy_vn(i, :) = (/control_space(max_row, :), &
! 			p_conflict(max_row), l_conflict(max_row), q(max_row), &
! 			D(max_row)/)
! 		value_fctn(i) = maxv

! 		value_fct(i) = max(value_fctd(i, 1), value_fctn(i))

! 		if ( value_fct(i)==value_fctd(i, 1) ) then
! 			policy_vv(i, :) = policy_vd(i, :)
! 		else
! 			policy_vv(i, :) = policy_vn(i, :)
! 		end if

! 		write(*, *) 'iteration', i, 'finished.'
! 	end do
! !$OMP END DO	
! !$OMP END PARALLEL

! 	call timestamp()

! 	XB_transpose_guo = transpose(XB)
! 	call dgemm('N','N',12000,12000,12000,1.0d0,XB_transpose_guo,12000,XB,12000,0.0d0,XB_product_guo,12000)
! 	call dgesv(12000, 12000, XB_product_guo, 12000, ipiv_guo, XB_transpose_guo, 12000, info_guo)	
! 	call dgemv('N',12000,12000,1.0d0,XB_transpose_guo,12000,value_fct,1,0.0d0,anew,1)
	
! 	write(*, *) 'enter to continue : [value_fct]'
! ! 			read(*, *) k
! 	write(*, *) (value_fct(k), k=450, 500)
! 			write(*, *) 'enter to continue : [value_fct]'
! ! 			read(*, *) k

! 	diff = norm(12000, aold(:)-anew(:))
! 	write(*, *) it, diff
! 	if ( diff <= 1e-6) exit

! end do

! 		start(1:fminsearchbnd_variable_num) = (/0.398580572626525d+00, 0.341640490822736d+00, 0.240413678727110d+00/) 
		start(1:fminsearchbnd_variable_num) = (/0.6d+00, -1.2d+00, 0.135d+00/) 
  		reqmin = 1.0D-08
  		step(1:fminsearchbnd_variable_num) = (/ 1.0D+00, 1.0D+00, 1.0D+00 /)
  		konvge = 10
  		kcount = 200000
!   		LB(1:fminsearchbnd_variable_num) = (/ smallB, smallM, small_b /)
! 		UB(1:fminsearchbnd_variable_num) = (/ bigB, bigM, bigb_conflict /)
		LB(1:fminsearchbnd_variable_num) = (/ -2d+00, -2d+00, -2d+00 /)
		UB(1:fminsearchbnd_variable_num) = (/ 3d+00, 3d+00, 3d+00 /)

		call read_file( 12000, aold )
! 		call fminsearchbnd (solve_mbBpe_sim3, fminsearchbnd_variable_num, start, &
! 			LB, UB, xmin, ynewlo, reqmin, step, &
! 		    konvge, kcount, icount, numres, ifault, &
! 		    zs(100, 1), B(100, 1), M(100, 1), &
! 			NZS, NB, NM, H, x, w_conflict, aold, &
! 			bigzs, smallzs, bigB, smallB, bigM, smallM, &
! 			gamma, rho_z, sigma_z, D_bar, W, P, Y, cf, &
! 			eps, beta, r_bar, f  )

		call fminsearchbnd(test_func, fminsearchbnd_variable_num, start, &
			LB, UB, xmin, ynewlo, reqmin, step, &
		    konvge, kcount, icount, numres, ifault)


		

! 			Y = solve_mbBpe_sim3(xmin, zs(100, 1), B(100, 1), M(100, 1), &
! 			NZS, NB, NM, H, x, w_conflict, aold, &
! 			bigzs, smallzs, bigB, smallB, bigM, smallM, &
! 			gamma, rho_z, sigma_z, D_bar, W, P, Y, cf, &
! 			eps, beta, r_bar, f  )
! 			Y = test_func(xmin)
		write(*, *) 'xmin = ', xmin
		write(*, *) 'ynewlo = ', ynewlo
		xmin = (/ 3.0d+00, -2.0d+00, 3.0d+00/)
		Y = test_func(xmin)
		write(*, *) 'actual = ', Y
		read(*, *) k


! 	write(*, *) "***** begin test_main ******"
! 	write(*, *) 'size of anew = ', size(anew, 1), &
! 		'w_conflict = ', size(w_conflict, 1), 'XBM = ', size(XBM, 1)
! 	write(*, *) "content of anew"
! 	write(*, *) ((anew(i, k), k=1, 1), i=1, 20)
! 	write(*, *) "***** end test_main *******"

deallocate(chzerozs, chzeroB, chzeroM)
deallocate(bendzs, bendB, bendM, zs, B, M)
deallocate(XBB, XBM, XBZS, XB_tmp_gqd, XB)
deallocate(x, w_conflict)
deallocate(B_space, M_space, b_space_conflict)
deallocate(B_nodes, M_nodes, b_nodes_conflict)
deallocate(control_space, B_control, M_control, b_control_conflict)
deallocate(anew)

deallocate( aold )
deallocate( value_fctd )
deallocate( policy_vd )
deallocate( temp_qbyB1, qbyB )
deallocate( q )
deallocate( i_B, i_b_conflict, cost ) ! should be type of integer
deallocate( p_conflict, y_conflict, l_conflict )
deallocate( iB, D, temp_valn1, temp_valn2 )
deallocate( policy_fctn, policy_vn, value_fctn )
deallocate( value_fct, policy_vv, temp_valn )
deallocate( XB_transpose_guo, XB_product_guo, ipiv_guo )

call timestamp()

contains

! subroutine test_kron ()
! 	use my_module
! 	implicit none
! 	real ( kind = 8 ), allocatable :: k(:, :)
! 	real ( kind = 8 ) :: a(2, 2), b(2, 2)
! 	integer ( kind = 4 ) :: i, j

! 	a(1, 1) = 1
! 	a(1, 2) = 2
! 	a(2, 1) = 3
! 	a(2, 2) = 4
! 	b(1, 1) = 1
! 	b(1, 2) = 0
! 	b(2, 1) = 0
! 	b(2, 2) = 1


! 	call kron(k, a, b)
! 	write(*, *) "***** begin test_kron ******"
! 	write(*, *) size(k,1), size(k,2)
! 	write(*, *) "content of k:"
! 	write(*, *) ((k(i, j), i=1, 4), j=1, 4)
! 	write(*, *) "***** end test_kron *******"
! 	deallocate(k)
! end subroutine test_kron

! subroutine test_gaucheby (m_conflict)
! 	use my_module
! 	implicit none
! 	integer ( kind = 4 ) :: i
! 	integer ( kind = 4 ), intent(in) :: m_conflict
! 	real ( kind = 8 ), allocatable :: x(:, :)
! 	call gaucheby(x, m_conflict)
! 	write(*, *) "***** begin test_gaucheby ******"
! 	write(*, *) "content of x"
! 	write(*, *) (x(i, 1), i=1, m_conflict)
! 	write(*, *) "***** end test_gaucheby *******"
! 	deallocate(x)
! end subroutine test_gaucheby

! subroutine test_chebeval ()
! 	use my_module
! 	implicit none
! 	integer ( kind = 4 ) :: row, colum
! 	integer ( kind = 4 ), parameter :: m_conflict = 4
! 	real ( kind = 8 ), allocatable :: guts(:, :)
! 	real ( kind = 8 ), allocatable :: X(:, :)

! 	write(*, *) "***** begin test_chebeval_1 ******"
! 	call gaucheby(guts, m_conflict)
! 	call chebeval(X, 5, guts)
! 	write(*, *) "content of x"
! 	do row = 1, 4
! 		write(*, *) (X(row, colum), colum=1, 5)
! 	end do
! 	write(*, *) "***** end test_chebeval_1 *******"
! 	deallocate(guts, X)

! 	write(*, *) "***** begin test_chebeval_2 ******"
! 	call chebeval(X, 5, 0.76158_8)
! 	write(*, *) "content of x"
! 	write(*, *) (X(1, colum), colum=1, 5)
! 	write(*, *) "***** end test_chebeval_2 *******"
! 	deallocate(X)
! end subroutine test_chebeval

! subroutine test_gauher ()
! 	use my_module
! 	implicit none
! 	integer ( kind = 4 ) :: i
! 	integer ( kind = 4 ), parameter :: n = 6
! 	real ( kind = 8 ), allocatable :: w_conflict(:), x(:)
! 	call gauher(x, w_conflict, n)

! 	write(*, *) "***** begin test_gauher ******"
! 	write(*, *) "content of x"
! 	write(*, *) (x(i), i=1, n)
! 	write(*, *) "content of w_conflict"
! 	write(*, *) (w_conflict(i), i=1, n)
! 	write(*, *) "***** end test_gauher *******"
! 	deallocate(x, w_conflict)
! end subroutine test_gauher

! subroutine test_norm ()
! 	use my_module
! 	implicit none
! 	integer ( kind = 4 ) :: n
! 	real ( kind = 8 ) :: vec(5) = (/1.0, 2.0, 3.0, 4.0, 5.0/)

! 	write(*, *) "***** begin test_norm ******"
! 	write(*, *) "norm(5, vec) = "
! 	write(*, *) norm(5, vec)
! 	write(*, *) "***** end test_norm *******"
! end subroutine test_norm

	subroutine test_solve_mbBpe_sim3 ()
		use util_module
		implicit none

! 		real ( kind = 8 ) :: h0n(3) = (/0.3289d+00, 0.2462d+00, 0.2952d+00/) 
		real ( kind = 8 ) :: h0n(3) = (/0.083975212355905d+00, 0.379281313790787d+00, 0.000006527687916d+00/) 
		real ( kind = 8 ) :: x(5) = (/-2.0202, -0.9586, .0, 0.9586, 2.0202/), &
			w(5) = (/0.02, 0.3936, 0.9453, 0.3936, 0.02/)
		real ( kind = 8 ), allocatable :: aold(:)

		call read_file( 12000, aold )
		y = solve_mbBpe_sim3(h0n, 1.7317d0, 1.0817d0, 1.0814d0, &
			5, 60, 40, 5, x, w, aold, &
			0.5774d0, -0.5774d0, 1.0819d0, 0d0, 1.0819d0, 0d0, &
			4, 0.5d0, 0.25d0, 0, 1, 1.3867d0, 0.2705d0, 0d0, &
			0.355d0, 0.9567d0, 0.04d0, 0.005d0)
		write(*, *) 'y = ', y
	end subroutine test_solve_mbBpe_sim3

! 	subroutine test_fminsearchbnd ()
! 		use util_module
! 		implicit none

! 		integer ( kind = 4 ), parameter :: n = 3
!   		integer ( kind = 4 ) :: i, icount, ifault, kcount, konvge, numres
! 		real ( kind = 8 ) :: reqmin, start(n), LB(n), UB(n), step(n), &
! 			xmin(n), ynewlo, y
! !   		real ( kind = 8 ), external :: solve_mbBpe_sim3

! 		real ( kind = 8 ) :: h0n(3) = (/0.1139D+00, 0.3416D+00, .0D+00/) 
! 		real ( kind = 8 ) :: x(5) = (/-2.0202D+00, -0.9586D+00, .0D+00, 0.9586D+00, 2.0202D+00/), &
! 			w(5) = (/0.02D+00, 0.3936D+00, 0.9453D+00, 0.3936D+00, 0.02D+00/)
! 		real ( kind = 8 ), allocatable :: aold(:)

! 		start(1:n) = (/0.3986d+00, 0.3416d+00, 0.2404d+00/) 
!   		reqmin = 1.0D-08
!   		step(1:n) = (/ 1.0D+00, 1.0D+00, 1.0D+00 /)
!   		konvge = 10
!   		kcount = 500
!   		LB(1:n) = (/ .0D+00, .0D+00, .0D+00 /)
! 		UB(1:n) = (/ 1.0819D+00, 1.0819D+00, 0.5409D+00 /)

! 		call read_file( 12000, aold )
! 		call fminsearchbnd (solve_mbBpe_sim3, n, start, LB, UB, xmin, ynewlo, reqmin, step, &
! 		    konvge, kcount, icount, numres, ifault, &
! 		    1.7317d0, 1.0772d0, 0.5622d0, &
! 			5, 60, 40, 5, x, w, aold, &
! 			0.5774d0, -0.5774d0, 1.0819d0, 0d0, 1.0819d0, 0d0, &
! 			4, 0.5d0, 0.25d0, 0, 1, 1.3867d0, 0.2705d0, 0d0, &
! 			0.355d0, 0.9567d0, 0.04d0, 0.005d0  )

! 		xmin = (/0.3289d+00, 0.2462d+00, 0.2952d+00/)

! 			y = solve_mbBpe_sim3(xmin, 1.7317d0, 1.0772d0, 0.5622d0, &
! 			5, 60, 40, 5, x, w, aold, &
! 			0.5774d0, -0.5774d0, 1.0819d0, 0d0, 1.0819d0, 0d0, &
! 			4, 0.5d0, 0.25d0, 0, 1, 1.3867d0, 0.2705d0, 0d0, &
! 			0.355d0, 0.9567d0, 0.04d0, 0.005d0)
! 		write(*, *) 'y = ', y
! 		write(*, *) 'xmin = ', xmin
! 		write(*, *) 'ynewlo = ', ynewlo
! 		write(*, *) 'actual = ', y
! 	end subroutine test_fminsearchbnd

end program main
