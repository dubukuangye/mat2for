module main_module
  use util_module

  integer ( kind = 4 ) :: H, NZS, B_size, M_size, b_size_conflict
  real ( kind = 8 ) :: rho_z, sigma_z, bigzs, smallzs, beta
  real ( kind = 8 ), allocatable :: zs(:, :), x(:), &
    B_control(:, :), M_control(:, :), b_control_conflict(:, :), &
    anew(:), aold(:), control_space(:, :), &
    w_conflict(:)

    !!$OMP THREADPRIVATE(temp_qbyB1, temp_valn2)

contains
  subroutine guo_223_314( i, temp_qbyB1, temp_valn2 )
    implicit none
    integer ( kind = 4 ) :: m, i
    real ( kind = 8 ) :: zsprime, zsnode
    real ( kind = 8 ), allocatable :: &
      zscv(:, :), xv_tmp1_guo(:, :), xv_tmp2_guo(:, :), xv(:, :), &
      i_vprime(:), vprime(:), i_bm(:), i_de(:), temp_qbyB(:), &
      vprime_tmp_guo(:)
    real ( kind = 8 ), intent(out) :: temp_qbyB1(:), temp_valn2(:)

    allocate( i_vprime(4000), i_bm(4000), i_de(4000) ) ! should be type of integer
    allocate( vprime(4000), temp_qbyB(B_size*M_size*b_size_conflict) )
    allocate( vprime_tmp_guo(4000) )
    do m=1, H
      zsprime = exp(rho_z*log(zs(i, 1)) + sigma_z*sqrt(2.0)*x(m))

!       % corresponding abscissas
      if ( abs(bigzs-smallzs)>=1e-3 ) then
        zsnode = 2*(log(zsprime)-smallzs)/(bigzs-smallzs)-1
      else
        zsnode = .0
      end if

      call chebeval(zscv, NZS, zsnode)
      call kron(xv_tmp1_guo, zscv, B_control)
      call kron(xv_tmp2_guo, xv_tmp1_guo, M_control)
      call kron(xv, xv_tmp2_guo, b_control_conflict)

      i_vprime = 0
      i_bm = 0
      vprime = matmul(xv, aold)
!       call dgemv('N',4000,12000,1.0d0,xv,4000,aold,1,0.0d0,vprime,1)
      where ( vprime(:)>=0 ) i_vprime(:) = 1
      where ( control_space(:,1)<=control_space(:,2) ) i_bm = 1
      i_de = ( 1 - i_vprime ) * ( 1 - i_bm )

      temp_qbyB = beta*(1-i_de)*control_space(:,1)+beta*i_de*control_space(:,2)
      temp_qbyB1 = temp_qbyB1+w_conflict(m)*temp_qbyB

      vprime_tmp_guo = matmul(xv, aold)
!       vprime_tmp_guo = vprime
!       call dgemv('N',4000,12000,1.0d0,xv,4000,aold,1,0.0d0,vprime_tmp_guo,1)
      temp_valn2 = temp_valn2 + w_conflict(m)*vprime_tmp_guo(:)

      deallocate( zscv, xv_tmp1_guo, xv_tmp2_guo, xv )
    end do
    deallocate( i_vprime, i_bm, i_de )
    deallocate( vprime, temp_qbyB )
    deallocate( vprime_tmp_guo )
  end subroutine guo_223_314

  subroutine guo_223( i, temp_qbyB1 )
    implicit none
    integer ( kind = 4 ) :: m, i
    real ( kind = 8 ) :: zsprime, zsnode
    real ( kind = 8 ), allocatable :: &
      zscv(:, :), xv_tmp1_guo(:, :), xv_tmp2_guo(:, :), xv(:, :), &
      i_vprime(:), vprime(:), i_bm(:), i_de(:), temp_qbyB(:)
    real ( kind = 8 ), intent(out) :: temp_qbyB1(:)
    
    allocate( i_vprime(4000), i_bm(4000), i_de(4000) ) ! should be type of integer
    allocate( vprime(4000), temp_qbyB(B_size*M_size*b_size_conflict) )

!     write(*, *) 'temp_qbyB1(450:500) :', temp_qbyB1(450:500)

    do m=1, H
      zsprime = exp(rho_z*log(zs(i, 1)) + sigma_z*sqrt(2.0)*x(m))

!       % corresponding abscissas
      if ( abs(bigzs-smallzs)>=1e-3 ) then
        zsnode = 2*(log(zsprime)-smallzs)/(bigzs-smallzs)-1
      else
        zsnode = .0
      end if

      call chebeval(zscv, NZS, zsnode)
      call kron(xv_tmp1_guo, zscv, B_control)
      call kron(xv_tmp2_guo, xv_tmp1_guo, M_control)
      call kron(xv, xv_tmp2_guo, b_control_conflict)

      i_vprime = 0
      i_bm = 0
      vprime = matmul(xv, aold)
!       call dgemv('N',4000,12000,1.0d0,xv,4000,aold,1,0.0d0,vprime,1)
      where ( vprime(:)>=0 ) i_vprime(:) = 1
      where ( control_space(:,1)<=control_space(:,2) ) i_bm = 1
      i_de = ( 1 - i_vprime ) * ( 1 - i_bm )

      temp_qbyB = beta*(1-i_de)*control_space(:,1)+beta*i_de*control_space(:,2)
      temp_qbyB1 = temp_qbyB1+w_conflict(m)*temp_qbyB


      deallocate( zscv, xv_tmp1_guo, xv_tmp2_guo, xv )
    end do
    deallocate( i_vprime, i_bm, i_de )
    deallocate( vprime, temp_qbyB )
  end subroutine

  subroutine guo_314( i, temp_valn2 )
    implicit none
    integer ( kind = 4 ) :: m, i
    real ( kind = 8 ) :: zsprime, zsnode
    real ( kind = 8 ), allocatable :: &
      zscv(:, :), xv_tmp1_guo(:, :), xv_tmp2_guo(:, :), xv(:, :), &
      vprime_tmp_guo(:)
    real ( kind = 8 ), intent(out) :: temp_valn2(:)
    
    allocate( vprime_tmp_guo(4000) )
    do m=1, H
      zsprime = exp(rho_z*log(zs(i, 1)) + sigma_z*sqrt(2.0)*x(m))

!       % corresponding abscissas
      if ( abs(bigzs-smallzs)>=1e-3 ) then
        zsnode = 2*(log(zsprime)-smallzs)/(bigzs-smallzs)-1
      else
        zsnode = 0
      end if

      call chebeval(zscv, NZS, zsnode)
      call kron(xv_tmp1_guo, zscv, B_control)
      call kron(xv_tmp2_guo, xv_tmp1_guo, M_control)
      call kron(xv, xv_tmp2_guo, b_control_conflict)

!       vprime_tmp_guo = matmul(xv, aold)
      call dgemv('N',4000,12000,1.0d0,xv,4000,aold,1,0.0d0,vprime_tmp_guo,1)
      temp_valn2 = temp_valn2 + w_conflict(m)*vprime_tmp_guo(:)
      
      deallocate( zscv, xv_tmp1_guo, xv_tmp2_guo, xv)
    end do
    deallocate( vprime_tmp_guo )
  end subroutine

end module main_module