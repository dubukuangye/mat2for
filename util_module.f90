module util_module
	interface chebeval
        module procedure chebeval_1, chebeval_2
  end interface chebeval

  real ( kind = 8 ), parameter :: pi = 3.1415926

contains
	subroutine read_file(n, mat)
    implicit none
    integer ( kind = 4 ) :: n
		real ( kind = 8 ), allocatable :: mat(:)
		integer ( kind = 4 ) :: i
		open(100, file='anew_guo.txt')
		allocate( mat(n) )
		read(100, *) (mat(i), i=1, n)
		close(100)
	end subroutine read_file

	subroutine kron(K, A, B)
	    implicit none
	    real ( kind = 8 ), intent(in)  :: A(:,:), B(:,:)
	    real ( kind = 8 ), allocatable :: K(:,:)
	    integer :: I, J, MA, NA, MB, NB
	    MA = UBOUND(A, 1)
	    NA = UBOUND(A, 2)
	    MB = UBOUND(B, 1)
	    NB = UBOUND(B, 2)
! 	    IF (SIZE(K,1) /= MA*MB .OR. SIZE(K,2) /= NA*NB) THEN
! 	        WRITE(*,*) 'K has invalid size'
! 	    !   CALL ABORT
! 	    END IF
	    allocate(K(MA*MB,NA*NB))
	    FORALL(I=1:MA, J=1:NA)
	        K(MB*(I-1)+1:MB*I,NB*(J-1)+1:NB*J) = A(I,J)*B
	    END FORALL
	end subroutine kron

	subroutine gaucheby(x, m)
		implicit none
		integer ( kind = 4 ) :: i
		integer ( kind = 4 ), intent(in) :: m
		real ( kind = 8 ) angle
		real ( kind = 8 ), parameter :: pi = 3.141592653589793D+00
		real ( kind = 8 ), allocatable :: x(:,:)

		allocate( x(m, 1) )
		do i = 1, m
	    	angle = real ( i-0.5, kind = 8 ) * pi / real ( m, kind = 8 )
	    	x(i, 1) = cos ( angle )
	 	end do
	end subroutine gaucheby

	subroutine chebeval_1(X, n, guts) ! n must not less than 2
		implicit none
		integer ( kind = 4 ) :: i, m
		integer ( kind = 4 ), intent(in) :: n
		real ( kind = 8 ), intent(in) :: guts(:, :)
		real ( kind = 8 ), allocatable :: X(:, :)

		m = size(guts, 1)
		allocate( X(m, n) )
		X = 0
		X(1:m, 1) = 1
		X(1:m, 2) = guts(1:m, 1)
		do i = 3, n
			X(1:m, i) = (2*guts(1:m, 1))*X(1:m, i-1) - X(1:m, i-2)
		end do
	end subroutine chebeval_1

	subroutine chebeval_2(X, n, guts) ! n must not less than 2
		implicit none
		integer ( kind = 4 ) :: i, m
		integer ( kind = 4 ), intent(in) :: n
		real ( kind = 8 ), intent(in) :: guts
		real ( kind = 8 ), allocatable :: X(:, :)

		m = 1
		allocate( X(m, n) )
		X = 0
		X(1:m, 1) = 1
		X(1:m, 2) = guts
		do i = 3, n
			X(1:m, i) = (2*guts)*X(1:m, i-1) - X(1:m, i-2)
		end do
	end subroutine chebeval_2

	subroutine gauher(x, w, n)  
		implicit none
     	integer ( kind = 4 ) ::  n, MAXIT = 10  
      	real ( kind = 8 ), allocatable ::  w(:),x(:)  
      	real ( kind = 8 ), parameter :: EPS = 3.D-14, PIM4 = .7511255444649425D0
     
      	integer ( kind = 4 ) :: i,its,j,m  
      	real ( kind = 8 ) :: p1,p2,p3,pp,z,z1  
      	m=(n+1)/2  

      	allocate(w(n), x(n))
      	do i=1, n
        	if( i.eq.1 ) then  
          		z=sqrt(real(2*n+1, kind=8))-1.85575*(2*n+1)**(-.16667)  
        	else if( i.eq.2 ) then  
          		z=z-1.14*n**.426/z  
        	else if( i.eq.3 ) then  
          		z=1.86*z-.86*x(1)  
        	else if( i.eq.4 ) then  
          		z=1.91*z-.91*x(2)  
        	else  
          		z=2.*z-x(i-2)  
        	endif

	        do its=1, MAXIT  
	          	p1=PIM4  
	          	p2=0.d0  
	          	do j=1, n  
	            	p3=p2  
	            	p2=p1  
	            	p1=z*sqrt(2.d0/j)*p2-sqrt(real(j-1, kind=8)/real(j, kind=8))*p3  
	        	end do 

	          	pp=sqrt(2.d0*n)*p2  
	          	z1=z  
	          	z=z1-p1/pp  
	          	if(abs(z-z1).le.EPS) exit
	        end do
			if( its>MAXIT ) then
				write(*, *) 'too many iterations in gauher' 
				stop 
			else
				x(i)=z  
		        x(n+1-i)=-z  
		        w(i)=2.d0/(pp*pp)  
		        w(n+1-i)=w(i)  
		    endif
		end do
    end subroutine gauher

    function norm(n, vec) result (norm_result)
      implicit none
    	real ( kind = 8 ) :: norm_result
    	real ( kind = 8 ) :: vec(:)
    	integer ( kind = 4 ) :: n

    	norm_result = sqrt( sum( vec(1:n)*vec(1:n) ) )
    end function norm

    function test_func(x) result (y)
      implicit none
      real ( kind = 8 ) :: x(3), y

      y = abs(x(1)) + abs(x(2)-1) - abs(x(1)+x(2)+x(3)-2)
!         y = x(1)**2 + 2.5d+00*sin( x(2) ) - x(3)**2*x(2)**2*x(1)**2
    end function test_func

    function solve_mbBpe_sim3(h,zss,Bs,Ms,NZS,NB,NM,H_conflict,x,w_conflict, &
      aold,bigzs,smallzs,bigB,smallB,bigM,smallM,gamma,rho_z, &
      sigma_z,D_bar,W,P,Y,cf,eps,beta,r_bar,f) result (y_conflict)
      implicit none
      real ( kind = 8 ) :: h(3), zss, Bs, Ms, &
        x(5), w_conflict(5), aold(:), &
        bigzs, smallzs, bigB, smallB, bigM, smallM, &
        rho_z, sigma_z, P, y_conflict, cf, eps, beta, r_bar, f, Y
      integer ( kind = 4 ) :: NZS, NB, NM, H_conflict, gamma, D_bar, W, m

      real ( kind = 8 ) :: temp_qbyB1, temp_qbyB, temp_valn2, temp_valn1, temp_val, vprime(1)
      real ( kind = 8 ), allocatable :: B_control(:, :), M_control(:, :), &
        zscv(:, :), xv_tmp1_guo(:, :), xv(:, :)
      real ( kind = 8 ) :: i_vprime, i_bm, i_de, vprime_tmp_guo(1), Bnode, Mnode, zsnode, &
        cost, D, i_B, i_b_conflict, iB, l_conflict, p_conflict, q, qbyB, zsprime

      Bnode = 2.0d0*(h(1)-smallB)/(bigB-smallB)-1
      Mnode = 2.0d0*(h(2)-smallM)/(bigM-smallM)-1
!       %bnode = 2*(h(3)-smallb)/(bigb-smallb)-1

      call chebeval(B_control, NB, Bnode)
      call chebeval(M_control, NM, Mnode)

      temp_qbyB1 = 0
      temp_valn2 = 0
      do m=1, H_conflict
        zsprime = exp(rho_z*log(zss) + sigma_z*sqrt(2.0d0)*x(m))

!       % corresponding abscissas
        if ( abs(bigzs-smallzs)>=1e-3 ) then
          zsnode = 2d0*(log(zsprime)-smallzs)/(bigzs-smallzs)-1
        else
          zsnode = .0
        end if

        call chebeval(zscv, NZS, zsnode)
        call kron(xv_tmp1_guo, zscv, B_control)
        call kron(xv, xv_tmp1_guo, M_control)

        vprime = matmul(xv, aold)
!       call dgemv('N',4000,12000,1.0d0,xv,4000,aold,1,0.0d0,vprime,1)
        i_vprime = 0
        i_bm = 0
        if ( vprime(1)>=0.0 ) i_vprime = 1
        if( h(1)<=h(2) ) i_bm = 1
        i_de = ( 1.0 - i_vprime ) * ( 1.0 - i_bm )

        temp_qbyB = beta*(1-i_de)*h(1)+beta*i_de*h(2)
        temp_qbyB1 = temp_qbyB1+w_conflict(m)*temp_qbyB

        vprime_tmp_guo = vprime
!         vprime_tmp_guo = matmul(xv, aold)
!       call dgemv('N',4000,12000,1.0d0,xv,4000,aold,1,0.0d0,vprime_tmp_guo,1)
        temp_valn2 = temp_valn2 + w_conflict(m)*vprime_tmp_guo(1)

        deallocate( zscv, xv_tmp1_guo, xv )
      end do

      temp_qbyB1 = temp_qbyB1 / sqrt( pi )
      qbyB = temp_qbyB1 * beta
      
!       q = qbyB / h(1)
      if ( h(1)==0 ) then
        qbyB = .0
        q = 1d-03
      else
        q = qbyB / h(1)
      end if

      if( q==0 ) q = 1d-03

!       write(*, *) 'q = ', q

      i_b_conflict = 0d0
      i_B = 0d0
      if ( h(3) > 0d0 ) i_b_conflict = 1d0
      if ( h(1) > 0d0 ) i_B = 1d0
      cost = r_bar*i_b_conflict*(1.0-i_B)+(1.0/q-1)*i_B
      p_conflict = gamma*1.0/(gamma-1.0)*(1+cost)*W/zss
      y_conflict = (P**gamma)*Y*(p_conflict**(-gamma))
      l_conflict = y_conflict / zss

      iB = 0
      if ( qbyB > 0 ) iB = 1
      D = p_conflict*y_conflict+(Ms+qbyB+h(3)- &
        W*l_conflict-cf-f*iB)-(1+r_bar)*h(3) - &
            Bs-h(2)

!       write(*, *) 'D = ', D

      temp_valn1 = D

      temp_valn2 = temp_valn2 / sqrt ( pi )
! write(*, *) 'temp_valn2 = ', temp_valn2      
      temp_val = temp_valn1 + temp_valn2*beta
!       write(*, *) 'temp_val [0] = ', temp_val
      if ( D < D_bar ) temp_val = -huge(0)
!       write(*, *) 'qbyB = ', qbyB, 'Ms = ', Ms, 'l_conflict = ', l_conflict, 'f = ', f 
!       write(*, *) 'qbyB+Ms-W*l_conflict = ', qbyB+Ms-W*l_conflict
!       write(*, *) 'left = ', qbyB+Ms &
!         -W*l_conflict-cf-f*iB
      if ( ( qbyB+Ms+h(3) &
        -W*l_conflict-cf-f*iB ) < .0d+00 ) &
        temp_val = -huge(0)
!       write(*, *) 'temp_val [2] = ', temp_val
      if ( h(3)>eps ) temp_val = -huge(0)  
!       write(*, *) 'temp_val [4] = ', temp_val

      y_conflict = -temp_val

      deallocate( B_control, M_control )
    end function solve_mbBpe_sim3

    subroutine fminsearchbnd(fn, n, start, LB, UB, xmin, ynewlo, reqmin, step, &
    konvge, kcount, icount, numres, ifault &
    )
!     zss,Bs,Ms,NZS,NB,NM,H_conflict,x,w_conflict, &
!     aold,bigzs,smallzs,bigB,smallB,bigM,smallM,gamma,rho_z, &
!     sigma_z,D_bar,W,P,Y,cf,eps,beta,r_bar,f )
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
! comments ...
! the original matrix a(n,n) will be destroyed
! during the calculation
!===========================================================
implicit none

! real ( kind = 8 ) :: zss, Bs, Ms, &
!   x(5), w_conflict(5), aold(:), &
!   bigzs, smallzs, bigB, smallB, bigM, smallM, &
!   rho_z, sigma_z, P, cf, eps, beta, r_bar, f, Y
! integer ( kind = 4 ) :: NZS, NB, NM, H_conflict, gamma, D_bar, W, m

integer ( kind = 4 ) n
real ( kind = 8 ), external :: fn
real ( kind = 8 ), parameter :: pi = 3.1415926
real ( kind = 8 ) :: start(n), LB(n), UB(n)
real ( kind = 8 ), intent(out) :: xmin(n)
real ( kind = 8 ), intent(out) :: ynewlo
real ( kind = 8 ) reqmin
real ( kind = 8 ) step(n)
integer ( kind = 4 ) icount
integer ( kind = 4 ) ifault
integer ( kind = 4 ) kcount
integer ( kind = 4 ) konvge
integer ( kind = 4 ) numres
integer ( kind = 4 ) i

real ( kind = 8 ) :: x0u(n)

do i=1, n
    x0u(i) = 2*(start(i) - LB(i))/(UB(i)-LB(i)) - 1
    x0u(i) = 2*pi+asin(max(real(-1, kind=8),min(real(1, kind=8),x0u(i))))
    step(i) = 1.0D+00
end do

! step(1) = 0.016443440285508D+00
! step(2) = 0.012308641273984D+00
! step(3) = 0.014759142338347D+00

reqmin = 1.0D-08

! write(*, *) 'before nelmin'

call nelmin ( fn, n, LB, UB, x0u, xmin, ynewlo, reqmin, step, &
    konvge, kcount, icount, numres, ifault )

! write(*, *) 'after nelmin'1

call xtransform(xmin, n, LB, UB)

contains
  subroutine nelmin ( fn, n, LB, UB, start, xmin, ynewlo, reqmin, step, konvge, kcount, &
    icount, numres, ifault )

  !*****************************************************************************80
  !
  !! NELMIN minimizes a function using the Nelder-Mead algorithm.
  !
  !  Discussion:
  !
  !    This routine seeks the minimum value of a user-specified function.
  !
  !    Simplex function minimisation procedure due to Nelder and Mead (1965),
  !    as implemented by O'Neill(1971, Appl.Statist. 20, 338-45), with
  !    subsequent comments by Chambers+Ertel(1974, 23, 250-1), Benyon(1976,
  !    25, 97) and Hill(1978, 27, 380-2)
  !
  !    The function to be minimized must be defined by a function of
  !    the form
  !
  !      function fn ( x, f )
  !      real ( kind = 8 ) fn
  !      real ( kind = 8 ) x(*)
  !
  !    and the name of this subroutine must be declared EXTERNAL in the
  !    calling routine and passed as the argument FN.
  !
  !    This routine does not include a termination test using the
  !    fitting of a quadratic surface.
  !
  !  Licensing:
  !
  !    This code is distributed under the GNU LGPL license.
  !
  !  Modified:
  !
  !    27 February 2008
  !
  !  Author:
  !
  !    Original FORTRAN77 version by R ONeill.
  !    FORTRAN90 version by John Burkardt.
  !
  !  Reference:
  !
  !    John Nelder, Roger Mead,
  !    A simplex method for function minimization,
  !    Computer Journal,
  !    Volume 7, 1965, pages 308-313.
  !
  !    R ONeill,
  !    Algorithm AS 47:
  !    Function Minimization Using a Simplex Procedure,
  !    Applied Statistics,
  !    Volume 20, Number 3, 1971, pages 338-345.
  !
  !  Parameters:
  !
  !    Input, external FN, the name of the function which evaluates
  !    the function to be minimized.
  !
  !    Input, integer ( kind = 4 ) N, the number of variables.
  !    0 < N is required.
  !
  !    Input/output, real ( kind = 8 ) START(N).  On input, a starting point
  !    for the iteration.  On output, this data may have been overwritten.
  !
  !    Output, real ( kind = 8 ) XMIN(N), the coordinates of the point which
  !    is estimated to minimize the function.
  !
  !    Output, real ( kind = 8 ) YNEWLO, the minimum value of the function.
  !
  !    Input, real ( kind = 8 ) REQMIN, the terminating limit for the variance
  !    of the function values.  0 < REQMIN is required.
  !
  !    Input, real ( kind = 8 ) STEP(N), determines the size and shape of the
  !    initial simplex.  The relative magnitudes of its elements should reflect
  !    the units of the variables.
  !
  !    Input, integer ( kind = 4 ) KONVGE, the convergence check is carried out
  !    every KONVGE iterations. 0 < KONVGE is required.
  !
  !    Input, integer ( kind = 4 ) KCOUNT, the maximum number of function
  !    evaluations.
  !
  !    Output, integer ( kind = 4 ) ICOUNT, the number of function evaluations
  !    used.
  !
  !    Output, integer ( kind = 4 ) NUMRES, the number of restarts.
  !
  !    Output, integer ( kind = 4 ) IFAULT, error indicator.
  !    0, no errors detected.
  !    1, REQMIN, N, or KONVGE has an illegal value.
  !    2, iteration terminated because KCOUNT was exceeded without convergence.
  !
    implicit none

    integer ( kind = 4 ) n

    real ( kind = 8 ), parameter :: ccoeff = 0.5D+00
    real ( kind = 8 ) del
    real ( kind = 8 ), parameter :: ecoeff = 2.0D+00
    real ( kind = 8 ), parameter :: eps = 0.001D+00
    real ( kind = 8 ), external :: fn
    integer ( kind = 4 ) i
    integer ( kind = 4 ) icount
    integer ( kind = 4 ) ifault
    integer ( kind = 4 ) ihi
    integer ( kind = 4 ) ilo
    integer ( kind = 4 ) j
    integer ( kind = 4 ) jcount
    integer ( kind = 4 ) kcount
    integer ( kind = 4 ) konvge
    integer ( kind = 4 ) l
    integer ( kind = 4 ) numres
    real ( kind = 8 ) p(n,n+1)
    real ( kind = 8 ) p2star(n)
    real ( kind = 8 ) pbar(n)
    real ( kind = 8 ) pstar(n)
    real ( kind = 8 ), parameter :: rcoeff = 1.0D+00
    real ( kind = 8 ) reqmin
    real ( kind = 8 ) rq
    real ( kind = 8 ) :: start(n), LB(n), UB(n)
    real ( kind = 8 ) step(n)
    real ( kind = 8 ) x
    real ( kind = 8 ) xmin(n)
    real ( kind = 8 ) y(n+1)
    real ( kind = 8 ) y2star
    real ( kind = 8 ) ylo
    real ( kind = 8 ) ynewlo
    real ( kind = 8 ) ystar
    real ( kind = 8 ) z
  !
  !  Check the input parameters.
  !
    if ( reqmin <= 0.0D+00 ) then
      ifault = 1
      return
    end if

    if ( n < 1 ) then
      ifault = 1
      return
    end if

    if ( konvge < 1 ) then
      ifault = 1
      return
    end if
  !
  !  Initialization.
  !
    icount = 0
    numres = 0
    jcount = konvge
    del = 1.0D+00
    rq = reqmin * real ( n, kind = 8 )
  !
  !  Initial or restarted loop.
  !
    do

      p(1:n,n+1) = start(1:n)
  !     y(n+1) = fn ( start )
      y(n+1) = intrafun(start, fn, n, LB, UB)
      icount = icount + 1
  !
  !  Define the initial simplex.
  !
      do j = 1, n
        x = start(j)
        start(j) = start(j) + step(j) * del
        p(1:n,j) = start(1:n)
  !       y(j) = fn ( start )
        y(j) = intrafun(start, fn, n, LB, UB)
        icount = icount + 1
        start(j) = x
      end do
  !
  !  Find highest and lowest Y values.  YNEWLO = Y(IHI) indicates
  !  the vertex of the simplex to be replaced.
  !
      ilo = minloc ( y(1:n+1), 1 )
      ylo = y(ilo)
  !
  !  Inner loop.
  !
      do while ( icount < kcount )
  !
  !  YNEWLO is, of course, the HIGHEST value???
  !
        ihi = maxloc ( y(1:n+1), 1 )
        ynewlo = y(ihi)
  !
  !  Calculate PBAR, the centroid of the simplex vertices
  !  excepting the vertex with Y value YNEWLO.
  !
        do i = 1, n
          pbar(i) = ( sum ( p(i,1:n+1) ) - p(i,ihi) ) / real ( n, kind = 8 )
        end do
  !
  !  Reflection through the centroid.
  !
        pstar(1:n) = pbar(1:n) + rcoeff * ( pbar(1:n) - p(1:n,ihi) )
  !       ystar = fn ( pstar )
        ystar = intrafun(pstar, fn, n, LB, UB)
        icount = icount + 1
  !
  !  Successful reflection, so extension.
  !
        if ( ystar < ylo ) then

          p2star(1:n) = pbar(1:n) + ecoeff * ( pstar(1:n) - pbar(1:n) )
  !         y2star = fn ( p2star )
          y2star = intrafun(p2star, fn, n, LB, UB)
          icount = icount + 1
  !
  !  Retain extension or contraction.
  !
          if ( ystar < y2star ) then
            p(1:n,ihi) = pstar(1:n)
            y(ihi) = ystar
          else
            p(1:n,ihi) = p2star(1:n)
            y(ihi) = y2star
          end if
  !
  !  No extension.
  !
        else

          l = 0
          do i = 1, n + 1
            if ( ystar < y(i) ) then
              l = l + 1
            end if
          end do

          if ( 1 < l ) then

            p(1:n,ihi) = pstar(1:n)
            y(ihi) = ystar
  !
  !  Contraction on the Y(IHI) side of the centroid.
  !
          else if ( l == 0 ) then

            p2star(1:n) = pbar(1:n) + ccoeff * ( p(1:n,ihi) - pbar(1:n) )
  !           y2star = fn ( p2star )
              y2star = intrafun(p2star, fn, n, LB, UB)
            icount = icount + 1
  !
  !  Contract the whole simplex.
  !
            if ( y(ihi) < y2star ) then

              do j = 1, n + 1
                p(1:n,j) = ( p(1:n,j) + p(1:n,ilo) ) * 0.5D+00
                xmin(1:n) = p(1:n,j)
  !               y(j) = fn ( xmin )
                  y(j) = intrafun(xmin, fn, n, LB, UB)
                icount = icount + 1
              end do

              ilo = minloc ( y(1:n+1), 1 )
              ylo = y(ilo)

              cycle
  !
  !  Retain contraction.
  !
            else
              p(1:n,ihi) = p2star(1:n)
              y(ihi) = y2star
            end if
  !
  !  Contraction on the reflection side of the centroid.
  !
          else if ( l == 1 ) then

            p2star(1:n) = pbar(1:n) + ccoeff * ( pstar(1:n) - pbar(1:n) )
  !           y2star = fn ( p2star )
              y2star = intrafun(p2star, fn, n, LB, UB)
            icount = icount + 1
  !
  !  Retain reflection?
  !
            if ( y2star <= ystar ) then
              p(1:n,ihi) = p2star(1:n)
              y(ihi) = y2star
            else
              p(1:n,ihi) = pstar(1:n)
              y(ihi) = ystar
            end if

          end if

        end if
  !
  !  Check if YLO improved.
  !
        if ( y(ihi) < ylo ) then
          ylo = y(ihi)
          ilo = ihi
        end if

        jcount = jcount - 1

        if ( 0 < jcount ) then
          cycle
        end if
  !
  !  Check to see if minimum reached.
  !
        if ( icount <= kcount ) then

          jcount = konvge

          x = sum ( y(1:n+1) ) / real ( n + 1, kind = 8 )
          z = sum ( ( y(1:n+1) - x )**2 )

          if ( z <= rq ) then
            exit
          end if

        end if

      end do
  !
  !  Factorial tests to check that YNEWLO is a local minimum.
  !
      xmin(1:n) = p(1:n,ilo)
      ynewlo = y(ilo)

      if ( kcount < icount ) then
        ifault = 2
        exit
      end if

      ifault = 0

      do i = 1, n
        del = step(i) * eps
        xmin(i) = xmin(i) + del
  !       z = fn ( xmin )
          z = intrafun(xmin, fn, n, LB, UB)
        icount = icount + 1
        if ( z < ynewlo ) then
          ifault = 2
          exit
        end if
        xmin(i) = xmin(i) - del - del
  !       z = fn ( xmin )
          z = intrafun(xmin, fn, n, LB, UB)
        icount = icount + 1
        if ( z < ynewlo ) then
          ifault = 2
          exit
        end if
        xmin(i) = xmin(i) + del
      end do

      if ( ifault == 0 ) then
        exit
      end if
  !
  !  Restart the procedure.
  !
      start(1:n) = xmin(1:n)
      del = eps
      numres = numres + 1

    end do

    return
  end subroutine nelmin

  subroutine xtransform(x, n, LB, UB)
    implicit none
    integer ( kind = 4 ) :: n, i
    real ( kind = 8 ) :: x(n), LB(n), UB(n)
    do i=1, n
        x(i) = (sin(x(i))+1)/2;
        x(i) = x(i)*(UB(i) - LB(i)) + LB(i)
        x(i) = max(LB(i),min(UB(i),x(i)))
    end do
  end subroutine xtransform

  function intrafun(x_intrafun, fn, n, LB, UB)
        implicit none
      integer ( kind = 4 ) i
      integer ( kind = 4 ) n
      real ( kind = 8 ) :: x_intrafun(n), xtrans(n), LB(n), UB(n)
      real ( kind = 8 ) :: intrafun
      real ( kind = 8 ), external :: fn

      do i=1, n
          xtrans(i) = (sin(x_intrafun(i))+1)/2;
          xtrans(i) = xtrans(i)*(UB(i) - LB(i)) + LB(i)
          xtrans(i) = max(LB(i),min(UB(i),xtrans(i)))
      end do

!       write(*, *) 'aold(1:10) = '
!       write(*, *) (aold(i), i=1, 10)

!       intrafun = fn(xtrans,zss,Bs,Ms,NZS,NB,NM,H_conflict,x,w_conflict, &
!         aold,bigzs,smallzs,bigB,smallB,bigM,smallM,gamma,rho_z, &
!         sigma_z,D_bar,W,P,Y,cf,eps,beta,r_bar,f)

! intrafun = solve_mbBpe_sim3(xtrans,zss,Bs,Ms,NZS,NB,NM,H_conflict,x,w_conflict, &
!         aold,bigzs,smallzs,bigB,smallB,bigM,smallM,gamma,rho_z, &
!         sigma_z,D_bar,W,P,Y,cf,eps,beta,r_bar,f)

      intrafun = test_func(xtrans)
  end function intrafun                 

    end subroutine fminsearchbnd

    subroutine timestamp ( )

!*****************************************************************************80
!
!! TIMESTAMP prints the current YMDHMS date as a time stamp.
!
!  Example:
!
!    31 May 2001   9:45:54.872 AM
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    18 May 2013
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    None
!
  implicit none

  character ( len = 8 ) ampm
  integer ( kind = 4 ) d
  integer ( kind = 4 ) h
  integer ( kind = 4 ) m
  integer ( kind = 4 ) mm
  character ( len = 9 ), parameter, dimension(12) :: month = (/ &
    'January  ', 'February ', 'March    ', 'April    ', &
    'May      ', 'June     ', 'July     ', 'August   ', &
    'September', 'October  ', 'November ', 'December ' /)
  integer ( kind = 4 ) n
  integer ( kind = 4 ) s
  integer ( kind = 4 ) values(8)
  integer ( kind = 4 ) y

  call date_and_time ( values = values )

  y = values(1)
  m = values(2)
  d = values(3)
  h = values(5)
  n = values(6)
  s = values(7)
  mm = values(8)

  if ( h < 12 ) then
    ampm = 'AM'
  else if ( h == 12 ) then
    if ( n == 0 .and. s == 0 ) then
      ampm = 'Noon'
    else
      ampm = 'PM'
    end if
  else
    h = h - 12
    if ( h < 12 ) then
      ampm = 'PM'
    else if ( h == 12 ) then
      if ( n == 0 .and. s == 0 ) then
        ampm = 'Midnight'
      else
        ampm = 'AM'
      end if
    end if
  end if

  write ( *, '(i2,1x,a,1x,i4,2x,i2,a1,i2.2,a1,i2.2,a1,i3.3,1x,a)' ) &
    d, trim ( month(m) ), y, h, ':', n, ':', s, '.', mm, trim ( ampm )

  return
end subroutine timestamp

end module util_module